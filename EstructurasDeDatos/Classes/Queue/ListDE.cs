﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructurasDeDatos.Classes.Queue
{
    public class ListDE
    {
        NodeDE start;
        int size;

        public bool Add(string data)
        {
            bool response = false;

            if (start == null)
            {
                start = new NodeDE(null, data, null);
                size++;
                response = true;
            }
            else
            {
                NodeDE aux = start;

                while (aux.HasNext())
                {
                    aux = aux.GetNext();
                }

                aux.SetNext(new NodeDE(aux, data, null));
                size++;
                response = true;
            }

            return response;
        }

        public void AddPosition(string data, int position)
        {
            NodeDE aux = start;
            NodeDE temp = new NodeDE(null, data, null);

            for (int i = 1; i < position; i++)
            {
                aux = aux.GetNext();
            }

            if (aux.GetPrevious() != null)
            {
                aux.GetPrevious().SetNext(temp);
                temp.SetPrevious(aux.GetPrevious());
                temp.SetNext(aux);
                aux.SetPrevious(temp);
            }
            else
            {
                temp.SetNext(aux);
                aux.SetPrevious(temp);
                start = temp;
            }
        }

        public int Quantity()
        {
            NodeDE aux = start;
            int response = 0;

            if (aux != null)
            {
                response++;

                while (aux.GetNext() != null)
                {
                    response++;
                    aux = aux.GetNext();
                }
            }            
            
            return response;
        }

        public NodeDE GetStart()
        {
            return start;
        }

        public void ProcessPerson()
        {
            NodeDE aux = start.GetNext();
            start = null;

            if (aux != null)
            {
                aux.SetPrevious(null);
                start = aux;
            }
        }
    }
}
