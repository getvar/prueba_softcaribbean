﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructurasDeDatos.Classes.Queue
{
    public class NodeDE
    {
        private NodeDE previous;
        private string data;
        private NodeDE next;

        public NodeDE(NodeDE previous, string data, NodeDE next)
        {
            this.previous = previous;
            this.data = data;
            this.next = next;
        }

        public NodeDE GetPrevious()
        {
            return previous;
        }

        public void SetPrevious(NodeDE previous)
        {
            this.previous = previous;
        }

        public string GetData()
        {
            return data;
        }

        public void SetData(string data)
        {
            this.data = data;
        }

        public NodeDE GetNext()
        {
            return next;
        }

        public void SetNext(NodeDE next)
        {
            this.next = next;
        }

        public bool HasNext()
        {
            bool response = false;

            if (this.next != null)
            {
                response = true;
            }

            return response;
        }

        public bool HasPrevious()
        {
            bool response = false;

            if (this.previous != null)
            {
                response = true;
            }

            return response;
        }
    }
}
