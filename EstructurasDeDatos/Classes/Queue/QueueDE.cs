﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EstructurasDeDatos.Classes.Queue
{
    public class QueueDE
    {
        ListDE persons = new ListDE();
        string value = string.Empty;

        public void ExecuteProcess()
        {
            int option = 0;
            string sep = "-----------------------------------------------------------------------------------------------------\n";

            while (option <= 0 || option > 6)
            {
                Console.Clear();
                Console.Write("----------------A continuación, se muestran las acciones que puede realizar:----------------\n" +
                              "1. Agregar persona a la fila\n" + sep +
                              "2. Atender una persona de la fila\n" + sep +
                              "3. Consultar la cantidad de personas pendientes por atender\n" + sep +
                              "4. Agregar persona colada\n" + sep +
                              "5. Atender a todas las personas de la fila\n" + sep +
                              "6. Mostrar las personas que están en la cola\n" + sep +
                              "m. Volver al menú principal\n\n" +
                              "Digite la acción que desea realizar:  ");
                value = Console.ReadLine().Trim();
                option = 0;

                if (value.ToUpper() == "m".ToUpper())
                {
                    return;
                }

                GenericHelper generic = new GenericHelper();

                if (!string.IsNullOrEmpty(value) && generic.IsInteger(value))
                {
                    option = Convert.ToInt32(value);
                }

                if (option <= 0 || option > 6)
                {
                    Console.Clear();
                    Console.Write("Opción incorrecta... Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            switch (option)
            {
                case 1:
                    AddPerson();
                    ExecuteProcess();
                    break;
                case 2:
                    ProcessPerson(false);
                    ExecuteProcess();
                    break;
                case 3:
                    Quantity();
                    ExecuteProcess();
                    break;
                case 4:
                    AddPersonPosition();
                    ExecuteProcess();
                    break;
                case 5:
                    ProcessPerson(true);
                    ExecuteProcess();
                    break;
                case 6:
                    ShowPersons();
                    ExecuteProcess();
                    break;
            }
        }

        private void ShowPersons()
        {
            Console.Clear();
            int quan = persons.Quantity();

            if (quan > 0)
            {
                NodeDE person, aux;
                person = persons.GetStart();
                aux = person;

                for (int i = 0; i < quan; i++)
                {
                    Console.WriteLine("Persona en cola: " + aux.GetData());
                    aux = aux.GetNext();
                }
            }
            else
            {
                Console.WriteLine("No existen personas en la cola.");
            }            

            AddPause();
        }

        private void AddPerson()
        {
            bool control = true;

            while (control)
            {
                Console.Clear();
                Console.Write("Ingrese la nombre de la persona: ");
                value = Console.ReadLine().Trim();

                if (!string.IsNullOrEmpty(value))
                {
                    control = false;
                    Add();
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("Debe ingresar una descripción. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }
        }

        private void Quantity()
        {
            int response = 0;
            response = persons.Quantity();
            Console.Clear();
            Console.WriteLine("La cantidad de personas pendientes por atender es: " + response);
            AddPause();
        }

        private void ProcessPerson(bool processAll)
        {
            Console.Clear();
            NodeDE person;
            string name = string.Empty;

            if (persons.Quantity() <= 0)
            {
                Console.WriteLine("No existen personas pendientes por atender.");
                AddPause();
            }
            else
            {
                if (!processAll)
                {
                    person = persons.GetStart();
                    name = person.GetData();
                    persons.ProcessPerson();
                    Console.WriteLine("Se ha atendido la persona: " + name);
                }
                else
                {
                    int quan = persons.Quantity();

                    for (int i = 0; i < quan; i++)
                    {
                        person = persons.GetStart();
                        name = person.GetData();
                        persons.ProcessPerson();
                        Console.WriteLine("Se ha atendido la persona: " + name);
                        Thread.Sleep(800);
                    }
                }

                AddPause();
            }
        }

        private void AddPersonPosition()
        {
            string name = string.Empty;
            bool control = true;
            int position = 0;

            while (control)
            {
                Console.Clear();
                Console.Write("Ingrese la nombre de la persona: ");
                value = Console.ReadLine().Trim();

                if (!string.IsNullOrEmpty(value))
                {
                    control = false;
                    name = value;

                    Console.Write("Ingrese la posición para la persona colada: ");
                    value = Console.ReadLine().Trim();
                    GenericHelper generic = new GenericHelper();

                    if (!string.IsNullOrEmpty(value) && generic.IsInteger(value))
                    {
                        position = Convert.ToInt32(value.ToString());

                        if (position <= 0)
                        {
                            control = true;
                            Console.Clear();
                            Console.Write("Resuerde que la posición debe ser un número mayor que cero (0). Intente de nuevo..");
                            AddPause();
                        }
                        else
                        {
                            int quan = persons.Quantity();

                            if (quan <= 0)
                            {
                                if (position > 1)
                                {
                                    Console.WriteLine("La posición: " + position.ToString() + " es incorrecta ya que la lista está vacía.");
                                    Thread.Sleep(1000);
                                    control = true;
                                }
                                else
                                {
                                    Add();
                                }
                            }
                            else
                            {
                                if (position > (quan + 1))
                                {
                                    Console.WriteLine("La posición: " + position.ToString() + " es incorrecta ya que la lista tiene solo: " + quan.ToString() + " posiciones.");
                                    Thread.Sleep(1000);
                                    control = true;
                                }
                                else if (position == (quan + 1))
                                {
                                    Add();
                                }
                                else
                                {
                                    persons.AddPosition(name, position);
                                    Console.WriteLine("\nPersona agregada con éxito.");
                                    AddPause();
                                }
                            }
                        }
                    }
                    else
                    {
                        control = true;
                        Console.Clear();
                        Console.Write("Valor incorrecto. Intente de nuevo..");
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("Debe ingresar una descripción. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }
        }

        private void Add()
        {
            persons.Add(value);
            Console.WriteLine("\nPersona agregada con éxito.");
            AddPause();
        }

        private void AddPause()
        {
            Console.Write("\nPresione Enter para volver al menú de la cola.");
            Console.ReadLine();
        }
    }
}
