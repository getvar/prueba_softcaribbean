﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructurasDeDatos.Classes.Tree
{
    public class MainTree
    {
        TreeNode root;

        public MainTree()
        {
            root = null;
        }

        public void AddNode(int data)
        {
            TreeNode newNode = new TreeNode(data);

            if (root == null)
            {
                root = newNode;
            }
            else
            {
                TreeNode aux = root;
                TreeNode father;
                bool control = true;

                while (control)
                {
                    father = aux;

                    if (data < aux.GetData())
                    {
                        aux = aux.GetLeft();

                        if (aux == null)
                        {
                            father.SetLeft(newNode);
                            control = false;
                        }
                    }
                    else
                    {
                        aux = aux.GetRight();

                        if (aux == null)
                        {
                            father.SetRight(newNode);
                            control = false;
                        }
                    }
                }
            }
        }

        public TreeNode GetRoot()
        {
            return root;
        }
    }
}
