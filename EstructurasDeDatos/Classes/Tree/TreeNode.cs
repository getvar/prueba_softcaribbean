﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructurasDeDatos.Classes.Tree
{
    public class TreeNode
    {
        private int data;
        private TreeNode left, right;

        public TreeNode(int data)
        {
            this.data = data;
            this.left = null;
            this.right = null;
        }

        public void SetLeft(TreeNode node)
        {
            this.left = node;
        }

        public void SetRight(TreeNode node)
        {
            this.right = node;
        }

        public TreeNode GetLeft()
        {
            return left;
        }

        public TreeNode GetRight()
        {
            return right;
        }

        public int GetData()
        {
            return data;
        }
    }
}
