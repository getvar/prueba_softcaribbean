﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EstructurasDeDatos.Classes.Tree
{
    public class clsTree
    {
        string value = string.Empty;
        MainTree tree = new MainTree();
        int quantity = 0;

        public void ExecuteProcess()
        {
            int option = 0;
            string sep = "-----------------------------------------------------------------------------------------------------\n";

            while (option <= 0 || option > 2)
            {
                Console.Clear();
                Console.Write("----------------A continuación, se muestran las acciones que puede realizar:----------------\n" +
                              "1. Agregar nodo al árbol\n" + sep +
                              "2. Recorrer árbol y contar nodos\n" + sep +
                              "m. Volver al menú principal\n\n" +
                              "Digite la acción que desea realizar:  ");
                value = Console.ReadLine().Trim();
                option = 0;

                if (value.ToUpper() == "m".ToUpper())
                {
                    return;
                }

                GenericHelper generic = new GenericHelper();

                if (!string.IsNullOrEmpty(value) && generic.IsInteger(value))
                {
                    option = Convert.ToInt32(value);
                }

                if (option <= 0 || option > 2)
                {
                    Console.Clear();
                    Console.Write("Opción incorrecta... Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            switch (option)
            {
                case 1:
                    AddNode();
                    ExecuteProcess();
                    break;
                case 2:
                    TreeNode root = tree.GetRoot();

                    if (root == null)
                    {
                        Console.WriteLine("El árbol está vacío.");
                        AddPause();
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("\nEl arbol recorrido InOrder es: \n");
                        quantity = 0;
                        InOrder(root);
                        Console.WriteLine("\nLa cantidad de nodos que tiene es: " + quantity.ToString());
                        AddPause();
                    }
                    
                    ExecuteProcess();
                    break;
            }
        }

        private void AddNode()
        {
            bool control = true;

            while (control)
            {
                Console.Clear();
                Console.Write("Ingrese el valor del nodo:  ");
                value = Console.ReadLine().Trim();
                GenericHelper generic = new GenericHelper();

                if (!string.IsNullOrEmpty(value) && generic.IsInteger(value))
                {
                    control = false;
                    tree.AddNode(Convert.ToInt32(value));
                    Console.WriteLine("\nNodo agregado con éxito.");
                    AddPause();
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("Valor incorrecto. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }
        }

        private void AddPause()
        {
            Console.Write("\nPresione Enter para volver al menú del árbol.");
            Console.ReadLine();
        }

        public void InOrder(TreeNode root)
        {
            if (root != null)
            {
                quantity++;
                InOrder(root.GetLeft());
                Console.WriteLine(root.GetData());
                InOrder(root.GetRight());
            }
        }
    }
}
