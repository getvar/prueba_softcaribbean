﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EstructurasDeDatos.Classes
{
    public class clsStack
    {
        Stack<string> transactions = new Stack<string>();

        public void ExecuteProcess()
        {
            int option = 0;
            string value = string.Empty, sep = "-----------------------------------------------------------------------------------------------------\n";

            while (option <= 0 || option > 5)
            {
                Console.Clear();
                Console.Write("----------------A continuación, se muestran las acciones que puede realizar:----------------\n" +
                              "1. Insertar transacción de venta a la pila\n" + sep +
                              "2. Procesar una transacción de venta\n" + sep +
                              "3. Consultar la cantidad de transacciones pendientes por procesar\n" + sep +
                              "4. Procesar todas las transacciones de venta (Vaciar pila)\n" + sep +
                              "5. Ver las transacciones pendientes\n" + sep +
                              "m. Volver al menú principal\n\n" +
                              "Digite la acción que desea realizar:  ");
                value = Console.ReadLine().Trim();
                option = 0;

                if (value.ToUpper() == "m".ToUpper())
                {
                    return;
                }

                GenericHelper generic = new GenericHelper();

                if (!string.IsNullOrEmpty(value) && generic.IsInteger(value))
                {
                    option = Convert.ToInt32(value);
                }

                if (option <= 0 || option > 5)
                {
                    Console.Clear();
                    Console.Write("Opción incorrecta... Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            switch (option)
            {
                case 1:
                    AddTransaction();
                    ExecuteProcess();
                    break;
                case 2:
                    ProcessTransactions(false);
                    ExecuteProcess();
                    break;
                case 3:
                    Quantity();
                    ExecuteProcess();
                    break;
                case 4:
                    ProcessTransactions(true);
                    ExecuteProcess();
                    break;
                case 5:
                    ShowTransactions();
                    ExecuteProcess();
                    break;
            }
        }

        private void ShowTransactions()
        {
            Console.Clear();
            int i = 1;

            foreach (string item in transactions)
            {
                Console.WriteLine("La transacción " + i.ToString() + " es: " + item);
                i++;
            }

            AddPause();
        }

        private void AddTransaction()
        {
            string value = string.Empty;
            bool control = true;

            while (control)
            {
                Console.Clear();
                Console.Write("Ingrese la identificación de la transacción: ");
                value = Console.ReadLine().Trim();

                if (!string.IsNullOrEmpty(value))
                {
                    control = false;
                    transactions.Push(value);
                    Console.WriteLine("\nTransacción agregada con éxito.");
                    Thread.Sleep(1000);
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("Debe ingresar una descripción. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }
        }

        private void ProcessTransactions(bool processAll)
        {
            string tran = string.Empty;
            Console.Clear();

            if (transactions == null || transactions.Count <= 0)
            {
                Console.WriteLine("No existen transacciones pendientes por procesar.");
                AddPause();
            }
            else
            {
                if (!processAll)
                {
                    tran = transactions.Pop();
                    Console.WriteLine("Se ha procesado la transacción: " + tran);
                    AddPause();
                }
                else
                {
                    int quan = transactions.Count;

                    for (int i = 0; i < quan; i++)
                    {
                        tran = transactions.Pop();
                        Console.WriteLine("Se ha procesado la transacción: " + tran);
                        Thread.Sleep(800);
                    }

                    Console.WriteLine("\nSe han procesado todas las transacciones pendientes.");
                    AddPause();
                }
            }
        }

        private void AddPause()
        {
            Console.Write("\nPresione Enter para volver al menú de transacciones.");
            Console.ReadLine();
        }

        private void Quantity()
        {
            int response = 0;
            response = transactions.Count;
            Console.Clear();
            Console.WriteLine("La cantidad de transacciones pendientes es: " + response);
            Console.Write("\nPresione Enter para volver al menú de transacciones.");
            Console.ReadLine();
        }
    }
}
