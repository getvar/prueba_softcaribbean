﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EvaluationToken.Classes
{
    public class Token
    {
        string value = string.Empty;

        public void ExecuteProcess()
        {
            bool control = true;

            while (control)
            {
                Console.Clear();
                Console.Write("Ingrese la cadena con las palabras claves  -  (ó digite la letra \"m\" para volver al menú principal):  ");
                value = Console.ReadLine().Trim();

                if (value.ToUpper() == "m".ToUpper())
                {
                    return;
                }

                if (!string.IsNullOrEmpty(value))
                {
                    control = false;
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("Debe ingresar el texto de la cadena. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            Calculate();
        }

        private void Calculate()
        {
            Console.Clear();
            string initial = "{", final = "}", f = string.Empty, response = string.Empty, aux = string.Empty;
            List<string> wordArray = new List<string>();
            bool control = true;
            f = value;

            while (control)
            {
                int start = f.IndexOf(initial) + initial.Length;

                if (start > initial.Length)
                {
                    int fin = 0;
                    aux = f;
                    f = f.Substring(start, f.Length - start);
                    fin = f.IndexOf(final);

                    if (fin != -1)
                    {
                        if (aux.Substring(start, fin).IndexOf(initial) == -1)
                        {
                            wordArray.Add(aux.Substring(start, fin));
                        }
                    }
                }
                else
                {
                    control = false;
                }
            }
            
            if (wordArray != null && wordArray.Count > 0)
            {
                Console.WriteLine("Las palabras clave encontradas son: \n");

                foreach (string item in wordArray)
                {
                    Console.WriteLine(item.First().ToString().ToUpper() + item.Substring(1));
                }
            }
            else
            {
                Console.WriteLine("No se encontraron palabras clave en la cadena ingresada.");
            }
            
            AddPause();
            ExecuteProcess();
        }

        private void AddPause()
        {
            Console.Write("\nPresione Enter para evaluar otra cadena o salir del ejercicio.");
            Console.ReadLine();
        }
    }
}
