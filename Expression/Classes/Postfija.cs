﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Expression.Classes
{
    public class Postfija
    {
        string value = string.Empty, response = string.Empty;
        Stack<string> symbolStack = new Stack<string>();

        public void ExecuteProcess()
        {
            bool control = true;

            while (control)
            {
                Console.Clear();
                symbolStack = new Stack<string>();
                response = string.Empty;
                Console.Write("Ingrese la expresión a evaluar  -  (ó digite la letra \"m\" para volver al menú principal):  ");
                value = Console.ReadLine().Trim();

                if (value.ToUpper() == "m".ToUpper())
                {
                    return;
                }

                if (!string.IsNullOrEmpty(value))
                {
                    control = false;
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("Debe ingresar la expresión. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            Calculate();
        }

        private void Calculate()
        {
            Console.Clear();
            string symbol = string.Empty, number = string.Empty;
            GenericHelper generic = new GenericHelper();

            foreach (char item in value)
            {
                if (generic.IsInteger(item.ToString()))
                {
                    number += item.ToString();
                }
                else
                {
                    if (!string.IsNullOrEmpty(number))
                    {
                        if (string.IsNullOrEmpty(response))
                        {
                            response = number;
                        }
                        else
                        {
                            response += "," + number;
                        }

                        number = string.Empty;
                    }
                                        
                    EvalStack(item.ToString());
                }
            }

            if (!string.IsNullOrEmpty(number))
            {
                if (string.IsNullOrEmpty(response))
                {
                    response = number;
                }
                else
                {
                    response += "," + number;
                }

                number = string.Empty;
                EvalStack(string.Empty);
            }
            else
            {
                EvalStack(string.Empty);
            }
            
            Console.WriteLine("La expresión infija: " + value + " en su forma postfija es: " + response);
            Console.WriteLine("\nEl resultado es: " + CalculateResponse().ToString());
            Console.Write("\nPresione Enter para cargar otro archivo o salir del ejercicio.");
            Console.ReadLine();
            ExecuteProcess();
        }

        private double CalculateResponse()
        {
            Stack<double> stack1 = new Stack<double>();
            string[] res = response.Split(',');
            GenericHelper generic = new GenericHelper();
            double total = 0;

            if (res != null)
            {
                foreach (string item in res)
                {
                    if (generic.IsInteger(item.ToString()))
                    {
                        stack1.Push(Convert.ToDouble(item.ToString()));
                    }
                    else
                    {
                        double number2 = stack1.Pop();
                        double number1 = stack1.Pop();
                        double r = operate(number1, number2, item.ToString());
                        stack1.Push(r);
                    }
                }

                total = stack1.Pop();
            }

            return total;
        }

        private double operate(double number1, double number2, string sym)
        {
            double resp = 0;

            switch (sym)
            {
                case "+":
                    resp = number1 + number2;
                    break;
                case "-":
                    resp = number1 - number2;
                    break;
                case "*":
                    resp = number1 * number2;
                    break;
                case "/":
                    resp = number1 / number2;
                    break;
                case "^":
                    resp = Math.Pow(number1, number2);
                    break;
            }

            return resp;
        }

        private void EvalStack(string sym)
        {
            string s = string.Empty;
            bool control = true;

            if (symbolStack.Count <= 0)
            {
                symbolStack.Push(sym);
            }
            else
            {
                if (sym == ")" || string.IsNullOrEmpty(sym))
                {
                    s = symbolStack.First();

                    while (symbolStack.Count > 0 && s != "(" && control)
                    {
                        if (string.IsNullOrEmpty(response))
                        {
                            response = symbolStack.Pop();
                        }
                        else
                        {
                            response += "," + symbolStack.Pop();
                        }

                        if (symbolStack.Count > 0)
                        {
                            s = symbolStack.First();

                            while (symbolStack.Count > 0 && s == "(")
                            {
                                symbolStack.Pop();

                                if (symbolStack != null && symbolStack.Count > 0)
                                {
                                    s = symbolStack.First();

                                    if (s != "(")
                                    {
                                        control = false;
                                    }
                                }                                
                            }
                        }
                    }
                }
                else
                {
                    if (sym == "(")
                    {
                        symbolStack.Push(sym);
                    }
                    else
                    {
                        if (symbolStack != null && symbolStack.Count > 0)
                        {
                            if (priority(sym) > priority(symbolStack.First()))
                            {
                                symbolStack.Push(sym);
                            }
                            else if (priority(sym) < priority(symbolStack.First()))
                            {
                                while (symbolStack.Count > 0)
                                {
                                    if (string.IsNullOrEmpty(response))
                                    {
                                        response = symbolStack.Pop();
                                    }
                                    else
                                    {
                                        response += "," + symbolStack.Pop();
                                    }
                                }

                                symbolStack.Push(sym);
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(response))
                                {
                                    response = symbolStack.Pop();
                                }
                                else
                                {
                                    response += "," + symbolStack.Pop();
                                }

                                symbolStack.Push(sym);
                            }
                        }
                    }
                }
            }
        }

        private int priority(string sym)
        {
            int response = 0;

            switch (sym)
            {
                case "(":
                    response = 0;
                    break;
                case "+":
                case "-":
                    response = 1;
                    break;
                case "*":
                case "/":
                    response = 2;
                    break;
                case "^":
                    response = 3;
                    break;
                default:
                    Console.WriteLine("La expresión no tiene el formato adecuado. Favor revisar y volver a intentar..");
                    AddPause();
                    ExecuteProcess();
                    break;
            }

            return response;
        }

        private void AddPause()
        {
            Console.Write("\nPresione Enter para evaluar otra expresión o salir del ejercicio.");
            Console.ReadLine();
        }
    }
}
