﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GroupEvaluation.Classes
{
    public class Evaluation
    {
        string value = string.Empty;
        string text = "[{ciudad:\"Medellin\",almacen:\"La 30\",mes:\"Enero\",venta:1000}," +
                "{ciudad:\"Medellin\",almacen:\"La 30\",mes:\"Febrero\",venta:800}," +
                "{ciudad:\"Medellin\",almacen:\"Los alpes\",mes:\"Enero\",venta:1200}," +
                "{ciudad:\"Medellin\",almacen:\"Los alpes\",mes:\"Febrero\",venta:1000}," +
                "{ciudad:\"Medellin\",almacen:\"Los alpes\",mes:\"Marzo\",venta:2000}," +
                "{ciudad:\"Cali\",almacen:\"La 30\",mes:\"Enero\",venta:500}," +
                "{ciudad:\"Cali\",almacen:\"La 30\",mes:\"Febrero\",venta:400}," +
                "{ciudad:\"Cali\",almacen:\"Los alpes\",mes:\"Enero\",venta:800}," +
                "{ciudad:\"Cali\",almacen:\"Los alpes\",mes:\"Febrero\",venta:700}," +
                "{ciudad:\"Cali\",almacen:\"Los alpes\",mes:\"Marzo\",venta:600}]";

        public class eSales
        {
            public string ciudad { get; set; }
            public string almacen { get; set; }
            public string mes { get; set; }
            public double venta { get; set; }
        }

        public class eResponse
        {
            public string city { get; set; }
            public string month { get; set; }
            public double total { get; set; }
        }

        public void ExecuteProcess()
        {
            int option = 0;
            string value = string.Empty, sep = "-----------------------------------------------------------------------------------------------------\n";

            while (option <= 0 || option > 2)
            {
                Console.Clear();
                Console.Write("----------------A continuación, se muestran las acciones que puede realizar:----------------\n" +
                              "1. Ingresar estructura Json por consola. ****Tener presente que para la consola de c# se permiten solo 254 caracteres****\n" + sep +
                              "2. Trabajar con estructura Json \"Quemada\" en el código\n" + sep +
                              "m. Volver al menú principal\n\n" +
                              "Digite la acción que desea realizar:  ");
                value = Console.ReadLine().Trim();
                option = 0;

                if (value.ToUpper() == "m".ToUpper())
                {
                    return;
                }

                GenericHelper generic = new GenericHelper();

                if (!string.IsNullOrEmpty(value) && generic.IsInteger(value))
                {
                    option = Convert.ToInt32(value);
                }

                if (option <= 0 || option > 2)
                {
                    Console.Clear();
                    Console.Write("Opción incorrecta... Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            switch (option)
            {
                case 1:
                    CalculateByEntry();
                    ExecuteProcess();
                    break;
                case 2:
                    Calculate(false);
                    ExecuteProcess();
                    break;
            }
        }

        private void CalculateByEntry()
        {
            bool control = true;

            while (control)
            {
                Console.Clear();
                Console.Write("Ingrese la estructura Json teniendo en cuenta que debe ser de 254 caracteres:  ");
                value = Console.ReadLine().Trim();

                if (!string.IsNullOrEmpty(value))
                {
                    control = false;
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("Debe ingresar la estructura Json. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            Calculate(true);
        }

        private void Calculate(bool entryText)
        {
            Console.Clear();
            List<eSales> sales = new List<eSales>();
            List<eResponse> responseList = new List<eResponse>();
            string aux = string.Empty;

            try
            {
                if (entryText)
                {
                    aux = value;
                }
                else
                {
                    aux = text;
                }

                if (aux.Contains("="))
                {
                    aux = aux.Replace("=", ":");
                }

                sales = Newtonsoft.Json.JsonConvert.DeserializeObject<List<eSales>>(aux);

                if (sales != null && sales.Count > 0)
                {
                    foreach (eSales item in sales)
                    {
                        if (responseList.Count <= 0)
                        {
                            eResponse resp = new eResponse()
                            {
                                city = item.ciudad,
                                month = item.mes,
                                total = item.venta
                            };

                            responseList.Add(resp);
                        }
                        else
                        {
                            if (!responseList.Exists(rl => rl.city == item.ciudad && rl.month == item.mes))
                            {
                                eResponse resp = new eResponse()
                                {
                                    city = item.ciudad,
                                    month = item.mes,
                                    total = item.venta
                                };

                                responseList.Add(resp);
                            }
                            else
                            {
                                responseList.Find(rl => rl.city == item.ciudad && rl.month == item.mes).total += item.venta; 
                            }
                        }
                    }

                    if (responseList != null && responseList.Count > 0)
                    {
                        responseList = responseList.OrderBy(rl => rl.month).ToList();
                        Console.WriteLine("El total de ventas por ciudad para cada mes es: \n");

                        foreach (eResponse item in responseList)
                        {
                            Console.WriteLine("Mes: " + item.month + " - Ciudad: " + item.city + " - total: " + item.total);
                        }
                    }
                    else
                    {
                        Console.WriteLine("No hay ventas realizadas.");
                    }
                }
                else
                {
                    Console.WriteLine("No hay ventas realizadas.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("La estructura Json ingresada no es la correcta.");
            }

            AddPause();
        }

        private void AddPause()
        {
            Console.Write("\nPresione Enter para evaluar otra estructura o salir del ejercicio.");
            Console.ReadLine();
        }
    }
}
