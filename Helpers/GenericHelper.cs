﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public class GenericHelper
    {
        public bool IsInteger(string value)
        {
            bool response = false;

            try
            {
                Int32 number = Convert.ToInt32(value);
                response = true;
            }
            catch
            {
                response = false;
            }

            return response;
        }

        public bool IsDouble(string value)
        {
            bool response = false;

            try
            {
                double number = Convert.ToDouble(value);
                response = true;
            }
            catch
            {
                response = false;
            }

            return response;
        }

        public bool ValidateFileExtension(string value, string extension)
        {
            bool response = false;

            if (value.Substring((value.Length - extension.Length), extension.Length) == extension)
            {
                response = true;
            }
            else
            {
                response = false;
            }

            return response;
        }
    }
}
