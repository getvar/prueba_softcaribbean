﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Miscelanea.Classes
{
    public class CalculateEx
    {
        int n = 0;
        double x = 0;

        public void ExecuteProcess_5A()
        {
            string value = string.Empty;
            bool control = true;

            while (control)
            {
                Console.Clear();
                Console.Write("Ingrese el valor de N. ***Recuerde que debe ser un número mayor que 0***" +
                              "  -  (ó digite la letra \"m\" para volver al menú principal):  ");
                value = Console.ReadLine().Trim();
                GenericHelper generic = new GenericHelper();
                n = 0;

                if (value.ToUpper() == "m".ToUpper())
                {
                    return;
                }
                
                if (!string.IsNullOrEmpty(value) && generic.IsInteger(value))
                {
                    n = Convert.ToInt32(value);
                    control = false;

                    if (n <= 0)
                    {
                        control = true;
                        Console.Clear();
                        Console.Write("Recuerde que debe ser un número mayor que cero (0)..");
                        Thread.Sleep(1000);
                    }

                    Console.Write("Ingrese el valor de X: ");
                    value = Console.ReadLine().Trim();

                    if (string.IsNullOrEmpty(value))
                    {
                        control = true;
                        Console.Clear();
                        Console.Write("Debe ingresar el valor de X");
                        Thread.Sleep(1000);
                    }
                    else
                    {
                        if (generic.IsInteger(value))
                        {
                            x = Convert.ToDouble(value);
                        }
                        else
                        {
                            control = true;
                            Console.Clear();
                            Console.Write("Debe ingresar un valor numérico");
                            Thread.Sleep(1000);
                        }
                    }
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("Valor incorrecto. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            Calculate(false);
        }

        public void ExecuteProcess_5B()
        {
            string value = string.Empty;
            bool control = true;

            while (control)
            {
                Console.Clear();
                Console.Write("Ingrese el valor de X - (ó digite la letra \"m\" para volver al menú principal):  ");
                value = Console.ReadLine().Trim();
                GenericHelper generic = new GenericHelper();
                x = 0;

                if (value.ToUpper() == "m".ToUpper())
                {
                    return;
                }

                if (!string.IsNullOrEmpty(value) && generic.IsInteger(value))
                {
                    x = Convert.ToInt32(value);
                    control = false;
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("Valor incorrecto. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            Calculate(true);
        }

        private void Calculate(bool withError)
        {
            double Ex = 0, Error = 0.001;
            int fac = 2;
            Ex = 1 + x;

            if (!withError)
            {
                while (fac <= n)
                {
                    Ex += Math.Pow(x, fac) / Factorial(fac);
                    fac++;
                }
            }
            else
            {
                x = 1;
                while ((Math.Pow(x,fac)/fac) >= Error)
                {
                    Ex += Math.Pow(x, fac) / Factorial(fac);
                    fac++;
                }
            }            
            
            Console.WriteLine("\nEl valor de Ex es: " + Ex);
            Console.Write("\nPresione Enter para calcular con otro valor de N o salir del ejercicio.");
            Console.ReadLine();

            if (!withError)
            {
                ExecuteProcess_5A();
            }
            else
            {
                ExecuteProcess_5B();
            }            
        }

        public double Factorial(int number)
        {
            int fac = 2;
            double response = 1;

            while (fac <= number)
            {
                response *= fac;
                fac++;
            }

            return response;
        }
    }
}
