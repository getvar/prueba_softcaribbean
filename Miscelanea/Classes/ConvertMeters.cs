﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Miscelanea.Classes
{
    public class ConvertMeters
    {
        double meters = 0, meterInch = 39.37, footInch = 12;

        public void ExecuteProcess()
        {
            string value = string.Empty;
            bool control = true;

            while (control)
            {
                Console.Clear();
                Console.Write("Ingrese la cantidad de metros a convertir  -  (ó digite la letra \"m\" para volver al menú principal):  ");
                value = Console.ReadLine().Trim();
                GenericHelper generic = new GenericHelper();
                meters = 0;

                if (value.ToUpper() == "m".ToUpper())
                {
                    return;
                }

                if (!string.IsNullOrEmpty(value) && generic.IsDouble(value))
                {
                    value = value.Replace(".", ",");
                    meters = Convert.ToDouble(value);
                    control = false;
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("Valor incorrecto. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            Calculate();
        }

        private void Calculate()
        {
            double inches = 0, foots = 0;
            inches = meters * meterInch;
            foots = inches / footInch;

            Console.WriteLine("\n" + meters + " Metros en pulgadas son: " + inches + " Pulgadas.\n" +
                              meters + " Metros en pies son: " + foots + " Pies.");
            Console.Write("\nPresione Enter para calcular con otro número o salir del ejercicio.");
            Console.ReadLine();
            ExecuteProcess();
        }
    }
}
