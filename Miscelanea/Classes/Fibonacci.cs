﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Miscelanea.Classes
{
    public class Fibonacci
    {
        int initialNumber = 3, n = 0;

        public void ExecuteProcess()
        {
            string value = string.Empty;
            bool control = true;

            while (control)
            {
                Console.Clear();
                Console.Write("Ingrese el número n-esimo que desea calcular. ***Recuerde que debe ser un número mayor o igual que 3***" +
                              "  -  (ó digite la letra \"m\" para volver al menú principal):  ");
                value = Console.ReadLine().Trim();
                GenericHelper generic = new GenericHelper();
                n = 0;

                if (value.ToUpper() == "m".ToUpper())
                {
                    return;
                }

                if (!string.IsNullOrEmpty(value) && generic.IsInteger(value))
                {
                    n = Convert.ToInt32(value);
                    control = false;

                    if (n < initialNumber)
                    {
                        control = true;
                        Console.Clear();
                        Console.Write("Recuerde que debe ser un número mayor o igual que " + initialNumber + "..");
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("Valor incorrecto. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            Calculate();
        }

        private void Calculate()
        {
            int response = 0;
            string serie = string.Empty;

            for (int i = 1; i <= n; i++)
            {
                response = CalculateFibonacci(i);

                if (string.IsNullOrEmpty(serie))
                {
                    serie = CalculateFibonacci(i).ToString();
                }
                else
                {
                    serie += ", " + CalculateFibonacci(i).ToString();
                }
            }

            Console.WriteLine("\nEl valor del n-ésimo término es: " + response);
            Console.WriteLine("\nLa serie es: " + serie);
            Console.Write("\nPresione Enter para calcular con otro número o salir del ejercicio.");
            Console.ReadLine();
            ExecuteProcess();
        }

        private int CalculateFibonacci(int num)
        {
            if (num == 0 || num == 1)
            {
                return num;
            }
            else
            {
                return CalculateFibonacci(num - 1) + CalculateFibonacci(num - 2);
            }
        }
    }
}
