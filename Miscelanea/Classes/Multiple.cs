﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Miscelanea.Classes
{
    public class Multiple
    {
        int initialNumber = 4, number = 0;

        public void ExecuteProcess()
        {
            string value = string.Empty;
            bool control = true;

            while (control)
            {
                Console.Clear();
                Console.Write("Ingrese el número hasta donde desea evaluar. ***Recuerde que debe ser un número mayor que 4***" +
                              "  -  (ó digite la letra \"m\" para volver al menú principal):  ");
                value = Console.ReadLine().Trim();
                GenericHelper generic = new GenericHelper();
                number = 0;

                if (value.ToUpper() == "m".ToUpper())
                {
                    return;
                }

                if (!string.IsNullOrEmpty(value) && generic.IsInteger(value))
                {
                    number = Convert.ToInt32(value);
                    control = false;
                    
                    if (number < initialNumber)
                    {
                        control = true;
                        Console.Clear();
                        Console.Write("Recuerde que debe ser un número mayor que 4..");
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("Valor incorrecto. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            Calculate();
        }

        private void Calculate()
        {
            string strMultiple = string.Empty;
            int quantity = 0;
            Console.WriteLine("\nEl valor ingresado es: " + number);

            for (int i = initialNumber; i <= number; i++)
            {
                if ((i % initialNumber) == 0)
                {
                    quantity++;
                    
                    if (string.IsNullOrEmpty(strMultiple))
                    {
                        strMultiple = i.ToString();
                    }
                    else
                    {
                        strMultiple += ", " + i.ToString();
                    }
                }
            }

            Console.WriteLine("La cantidad de múltiplos de " + initialNumber + " que hay entre: (" + initialNumber + " y " + number + ") es: " + quantity +
                              ". La lista es: " + strMultiple);
            Console.Write("\nPresione Enter para calcular con otro número o salir del ejercicio.");
            Console.ReadLine();
            ExecuteProcess();
        }
    }
}
