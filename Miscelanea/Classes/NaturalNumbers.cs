﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Miscelanea.Classes
{
    public class NaturalNumbers
    {
        bool control = false;

        public void ExecuteProcess()
        {
            string value = string.Empty;
            

            while (control)
            {
                Console.Clear();
                Console.Write("Digite cero (0) para calcular nuevamente  -  (ó digite la letra \"m\" para volver al menú principal):  ");
                value = Console.ReadLine().Trim();
                GenericHelper generic = new GenericHelper();

                if (value.ToUpper() == "m".ToUpper())
                {
                    return;
                }

                if (!string.IsNullOrEmpty(value) && generic.IsInteger(value))
                {
                    if (value == "0")
                    {
                        control = false;
                    }
                    else
                    {
                        control = true;
                        Console.Clear();
                        Console.Write("Valor incorrecto. Intente de nuevo..");
                        Thread.Sleep(1000);
                    }                    
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("Valor incorrecto. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            Console.Clear();
            Calculate();
        }

        private void Calculate()
        {
            int response = 0;
            int n = 100;
            response = ((n * (n + 1) * ((2 * n) + 1)) / 6);
            Console.WriteLine("La suma de los cuadrados de los primeros 100 números naturales es: " + response.ToString());
            Console.Write("\nPresione Enter para mostrar opciones.");
            Console.ReadLine();
            control = true;
            ExecuteProcess();
        }
    }
}
