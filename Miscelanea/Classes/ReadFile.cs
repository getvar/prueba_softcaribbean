﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Miscelanea.Classes
{
    public class ReadFile
    {
        string path = string.Empty;

        public void ExecuteProcess()
        {
            string value = string.Empty, extension = ".txt";
            bool control = true;

            while (control)
            {
                Console.Clear();
                Console.Write("Ingrese la ruta del archivo con el nombre y extensión de este (Debe ser un archivo .txt)  -  (ó digite la letra \"m\" para volver al menú principal):  ");
                value = Console.ReadLine().Trim();
                GenericHelper generic = new GenericHelper();

                if (value.ToUpper() == "m".ToUpper())
                {
                    return;
                }

                if (!string.IsNullOrEmpty(value) && value.Length > extension.Length && generic.ValidateFileExtension(value, extension))
                {
                    path = value;
                    control = false;
                }
                else
                {
                    control = true;
                    Console.Clear();
                    Console.Write("La ruta ingresada es incorrecta. Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            Calculate();
        }

        private void Calculate()
        {
            int evenSum = 0, primeSum = 0;
            string content = System.IO.File.ReadAllText(path);
            GenericHelper generic = new GenericHelper();

            if (string.IsNullOrEmpty(content))
            {
                Console.Write("El archivo está vacío. Favor verificar y volver a intentar..");
                Thread.Sleep(2000);
                ExecuteProcess();
            }
            else
            {
                foreach (char item in content)
                {
                    if (generic.IsInteger(item.ToString()))
                    {
                        int num = Convert.ToInt32(item.ToString());

                        if (isPrime(num))
                        {
                            primeSum += num;
                        }

                        if (isEven(num))
                        {
                            evenSum += num;
                        }
                    }
                    else
                    {
                        Console.Write("El archivo no tiene la estructura adecuada, tiene elementos diferentes a números. Favor verificar y volver a intentar..");
                        Thread.Sleep(2000);
                        ExecuteProcess();
                    }
                }
            }

            Console.WriteLine("\nLa suma de los números pares del archivo es: " + evenSum + "\n" +
                              "La suma de los números primos del archivo es: " + primeSum);
            Console.Write("\nPresione Enter para cargar otro archivo o salir del ejercicio.");
            Console.ReadLine();
            ExecuteProcess();
        }

        private bool isPrime(int number)
        {
            bool response = true;
            int divisor = 2;

            if (number < 2)
            {
                response = false;
            }
            else
            {
                while (response && (divisor <= Math.Sqrt(Convert.ToDouble(number))))
                {
                    if ((number % divisor) == 0)
                    {
                        response = false;
                    }

                    divisor++;
                }
            }

            return response;
        }

        private bool isEven(int number)
        {
            bool response = false;

            if ((number % 2) == 0)
            {
                response = true;
            }

            return response;
        }
    }
}
