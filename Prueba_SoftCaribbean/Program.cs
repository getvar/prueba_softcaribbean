﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Miscelanea.Classes;
using EstructurasDeDatos.Classes;
using EstructurasDeDatos.Classes.Queue;
using EstructurasDeDatos.Classes.Tree;
using EvaluationToken.Classes;
using GroupEvaluation.Classes;
using Expression.Classes;

namespace Prueba_SoftCaribbean
{
    class Program
    {
        static void Main(string[] args)
        {
            MainMethod();
        }

        private static void MainMethod()
        {
            int option = 0;
            string value = string.Empty, sep = "-----------------------------------------------------------------------------------------------------\n";

            while (option <= 0 || option > 14)
            {
                Console.Clear();
                Console.Write("----------------A continuación, se muestran las acciones de los ejercicios a evaluar:----------------\n" +
                              "1. Calcular los múltiplos de 4 comprendidos entre 4 y N , donde N es un valor introducido por consola\n" + sep +
                              "2. Convertir metros a pies y a pulgadas\n" + sep +
                              "3. Calcular la suma de los cuadrados de los 100 primeros números naturales\n" + sep +
                              "4. Leer un archivo que solo contiene números y sumar los números pares y primos\n" + sep +
                              "5. (5A) Calcular Ex para N dado\n" + sep +
                              "6. (5B) Calcular Ex hasta que se cumpla la condición del error\n" + sep +
                              "7. Calcular el n-ésimo término de la serie de Fibonacci\n" + sep +
                              "8. Implemente una Pila que opere de manera LIFO(Last in, first out) para recibir simulación de transacciones de ventas\n" + sep +
                              "9. Teniendo una cola doblemente enlazada de personas haciendo la fila en un banco, permita adicionar una nueva persona que se mete de colada a la fila\n" + sep +
                              "10. Escriba un programa que recorra un árbol binario de números enteros y cuente cuantos nodos tiene\n" + sep +
                              "11. Teniendo una cadena con varias palabras claves entre corchetes {}, encuentra dichas palabras y retorne un Array con dichas palabras, " +
                                   "colocando su primera letra en mayúscula\n" + sep +
                              "12. Realizar la evaluación de una expresión matemática usando evaluación postfija\n" + sep +
                              "13. Teniendo un arreglo en una estructura Json, según muestra la figura. Calcule y muestre el total por ciudad para cada mes\n" + sep +
                              "14. Salir del programa\n\n" +
                              "Digite la opción del ejercicio a evaluar:  ");
                value = Console.ReadLine().Trim();
                option = 0;
                GenericHelper generic = new GenericHelper();

                if (!string.IsNullOrEmpty(value) && generic.IsInteger(value))
                {
                    option = Convert.ToInt32(value);
                }

                if (option <= 0 || option > 14)
                {
                    Console.Clear();
                    Console.Write("Opción incorrecta... Intente de nuevo..");
                    Thread.Sleep(1000);
                }
            }

            switch (option)
            {
                case 1:
                    Multiple mul = new Multiple();
                    mul.ExecuteProcess();
                    MainMethod();
                    break;
                case 2:
                    ConvertMeters met = new ConvertMeters();
                    met.ExecuteProcess();
                    MainMethod();
                    break;
                case 3:
                    NaturalNumbers nat = new NaturalNumbers();
                    nat.ExecuteProcess();
                    MainMethod();
                    break;
                case 4:
                    ReadFile read = new ReadFile();
                    read.ExecuteProcess();
                    MainMethod();
                    break;
                case 5:
                    CalculateEx Ex_A = new CalculateEx();
                    Ex_A.ExecuteProcess_5A();
                    MainMethod();
                    break;
                case 6:
                    CalculateEx Ex_B = new CalculateEx();
                    Ex_B.ExecuteProcess_5B();
                    MainMethod();
                    break;
                case 7:
                    Fibonacci fib = new Fibonacci();
                    fib.ExecuteProcess();
                    MainMethod();
                    break;
                case 8:
                    clsStack stack = new clsStack();
                    stack.ExecuteProcess();
                    MainMethod();
                    break;
                case 9:
                    QueueDE queue = new QueueDE();
                    queue.ExecuteProcess();
                    MainMethod();
                    break;
                case 10:
                    clsTree tree = new clsTree();
                    tree.ExecuteProcess();
                    MainMethod();
                    break;
                case 11:
                    Token tok = new Token();
                    tok.ExecuteProcess();
                    MainMethod();
                    break;
                case 12:
                    Postfija post = new Postfija();
                    post.ExecuteProcess();
                    MainMethod();
                    break;
                case 13:
                    Evaluation eval = new Evaluation();
                    eval.ExecuteProcess();
                    MainMethod();
                    break;
                case 14:
                    Console.Clear();
                    Console.Write("Ha finalizado el programa.");
                    Thread.Sleep(1000);
                    return;
            }
        }
    }
}
